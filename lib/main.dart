import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
   Widget build(BuildContext context) {
    Widget titleHeader = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('California State University San Marcos(CSUSM)', style: TextStyle(
                    fontWeight: FontWeight.bold,),
                    ),
                    ),
                    Text('Public university in California', style: TextStyle(
                      color: Colors.grey[400],),
                    ),
              ],
              ),
              ),
              ],
              ),
    );
    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        buttonColumn(color, Icons.people, '~15,000 students'),
        buttonColumn(color, Icons.ac_unit, 'has AC'),
        buttonColumn(color, Icons.local_library, 'has library'),
        buttonColumn(color, Icons.contact_phone , '(760) 750-4000'),
      ],
    )
  );

Widget textDescription = Container(
  padding: const EdgeInsets.all(32),
  child: Text(
    'CSUSM is an excellent school choice, having great professors and students alike! '
    'There is 5-story library as well as a food court which is a popular destination for breaks inbetween classes. '
    'However, there is a lot of stairs, so get ready to work those glutes walking up and down stairs all day! '
    'Did you know? CSUSM is aiming for zero waste by 2025 in an effort to create a sustainable enviroment.',
    softWrap: true,
  ),
);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(), //enable dark mode
      title: 'My Cool App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('School Directory'),
        ),
        body: ListView(
          children: [
            Image.asset('images/csusm.jpg',
            width: 600,
            height: 240,
            fit: BoxFit.cover,
            ),
            titleHeader,
            buttonSection,
            textDescription
          ]
          ),
        )
      );
   }

  Column buttonColumn(Color color, IconData icon, String label){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
          ),
      ],
    );
  }

} //MyApp